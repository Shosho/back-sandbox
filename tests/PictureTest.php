<?php

namespace App\Tests;

use App\Entity\Picture;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class PictureTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManager
     */
    private $em;

    public function setUp(): void
    {
        $this->client = static::createClient();

        // We avoid inserting data in the db
        $this->em = self::$container->get('doctrine')
            ->getManager();
        $this->em->getConfiguration()->setAutoCommit(false);
        $this->em->beginTransaction();
    }

    public function testCreatePictureViolation(): void
    {
        copy(__DIR__.'/test.png', __DIR__.'/tmp.png');
        $file = new UploadedFile(
            __DIR__.'/tmp.png',
            'test.png',
            'image/png',
            null
        );

        $this->client->request(
            'POST',
            '/pictures',
            [
                'title' => '',
                'description' => '',
            ],
            [
                'file' => $file,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
    }

    public function testCreatePicture(): void
    {
        copy(__DIR__.'/test.png', __DIR__.'/tmp.png');
        $file = new UploadedFile(
            __DIR__.'/tmp.png',
            'test.png',
            'image/png',
            null
        );

        $this->client->request(
            'POST',
            '/pictures',
            [
                'title' => 'Hello World',
                'description' => 'This is a test',
            ],
            [
                'file' => $file,
            ]
        );

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testGetPictures(): void
    {
        $this->client->request(
            'GET',
            '/pictures'
        );

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testDeletePicture(): void
    {
        $picture = (new Picture())
            ->setName('tmp')
            ->setPath('tmp')
            ->setDescription('tmp');

        $this->em->persist($picture);
        $this->em->flush();

        $this->assertNotNull($picture->getId());

        $this->client->request(
            'DELETE',
            '/pictures/'.$picture->getId()
        );

        $this->assertNull($picture->getId());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }
}
