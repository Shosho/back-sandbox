<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Service\PictureService;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PictureController extends AbstractController
{
    /**
     * @Route(
     *     "/pictures",
     *     methods={"GET"}
     * )
     */
    public function index(): Response
    {
        return new Response(
            (new SerializerBuilder())
                ->build()
                ->serialize(
                    $this->getDoctrine()->getRepository(Picture::class)->findAll(),
                    'json'
                )
        );
    }

    /**
     * @Route(
     *     "/pictures/{id}",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function remove(Picture $picture): Response
    {
        $this->getDoctrine()->getManager()->remove($picture);
        $this->getDoctrine()->getManager()->flush();

        return $this->forward(PictureController::class.'::index');
    }

    /**
     * @Route(
     *     "/pictures",
     *     methods={"POST"}
     * )
     */
    public function add(PictureService $service): Response
    {
        $service->uploadPicture();

        return $this->forward(PictureController::class.'::index');
    }
}
