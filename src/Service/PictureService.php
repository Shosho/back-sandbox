<?php

namespace App\Service;

use App\Entity\Picture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PictureService extends AbstractService
{
    /**
     * @var string
     */
    private $picturesDirectory;
    /**
     * @var string
     */
    private $urlApi;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $request_stack,
        string $pictures_directory,
        string $url_api
    ) {
        parent::__construct($em, $request_stack);
        $this->picturesDirectory = $pictures_directory;
        $this->urlApi = $url_api;
    }

    public function uploadPicture(): void
    {
        if (empty($this->request->get('title'))) {
            throw new BadRequestHttpException('Please add a title before submitting.');
        }

        if (empty($this->request->get('description'))) {
            throw new BadRequestHttpException('Please add a description before submitting.');
        }

        $file = $this->request->files->get('file');

        if (!$file instanceof UploadedFile) {
            throw new BadRequestHttpException('Please add a file before submitting.');
        }

        $picture = (new Picture())
            ->setName($this->request->get('title'))
            ->setSubtitle($this->request->get('subtitle'))
            ->setDescription($this->request->get('description'))
            ->setPath($this->urlApi.'uploads/pictures/'.$file->getClientOriginalName());

        $this->em->persist($picture);
        $this->em->flush();

        // We move the file at the end in case there's an error with the picture's insertion
        $file->move(
            $this->picturesDirectory,
            $file->getClientOriginalName()
        );
    }
}
