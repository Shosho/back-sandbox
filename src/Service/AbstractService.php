<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class AbstractService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var Request|null
     */
    protected $request;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $request_stack
    ) {
        $this->em = $em;
        $this->request = $request_stack->getCurrentRequest();
    }
}
