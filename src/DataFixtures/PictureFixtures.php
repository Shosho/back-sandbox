<?php

namespace App\DataFixtures;

use App\Entity\Picture;

class PictureFixtures extends BaseFixture
{
    protected function loadData(): void
    {
        $this->createMany(Picture::class, 5, function (Picture $picture, int $count) {
            $picture->setName($this->faker->name)
                ->setPath($this->faker->imageUrl())
                ->setDescription($this->faker->text())
                ->setSubtitle(
                    3 === $count ?
                        null :
                        $this->faker->text(10)
                );
        });

        $this->em->flush();
    }
}
