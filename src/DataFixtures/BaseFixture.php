<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

abstract class BaseFixture extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $em;
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    public function load(ObjectManager $em): void
    {
        $this->em = $em;
        $this->faker = Factory::create();
        $this->loadData();
    }

    protected function createMany(string $className, int $count, callable $factory): void
    {
        for ($i = 0; $i < $count; ++$i) {
            $entity = new $className();
            $factory($entity, $i);
            $this->em->persist($entity);
            $this->addReference($className.'_'.$i, $entity);
        }
    }

    abstract protected function loadData(): void;
}
